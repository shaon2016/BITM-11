<?php

/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 6/4/2016
 * Time: 6:14 PM
 */
class Car
{
    private $brand;
    private $color;
    private $speed;
    private $capacityToContainPeople;

    function __construct($color, $speed, $capacityToContainPeople,
            $brand)
    {
        $this->brand = $brand;
        $this->color = $color;
        $this->speed = $speed;
        $this->capacityToContainPeople = $capacityToContainPeople;

        echo "<b>"."constructing ". $this->getBrandName().
            " student"."</b>"."<br>";
    }

    function getBrandName() {
        return $this->brand;
    }

    function getColorOfCar() {
        return $this->color;
    }

    function getSpeedOfCar() {
        return $this->speed;
    }

    function getCapacityToContainPeopleCar() {
        return $this->capacityToContainPeople;
    }
    
    function __destruct()
    {
        echo "<b>"."desstructing ". $this->getBrandName().
            " student"."</b>"."<hr>";
    }
}