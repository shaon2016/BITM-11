<?php

namespace people;

class Student
{
    private $id;
    private $name;
    
    function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;

        echo "<b>"."constructing ". $this->getId().
            " student"."</b>"."<br>";
        
    }
    
    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    function __destruct()
    {
        echo "<b>"."desstructing ". $this->getName().
            " student"."</b>"."<hr>";
    }
}

